﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace HelloWorld.Server
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service1" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select Service1.svc or Service1.svc.cs at the Solution Explorer and start debugging.
    public class HelloWorld : IHelloWorld
    {
        public int AddTwoNumbers(int a, int b)
        {
            //test  entry sss
            // new test
            return a + b;
        }

        // ms build test 
        // restest 2
        // POLL SCM
        public int DivideTwoNumber(int a, int b)
        {
            return b / a;
        }

        public string GetData(int value)
        {
            return string.Format("You entered: {0}", value);
        }

        public CompositeType GetDataUsingDataContract(CompositeType composite)
        {
            if (composite == null)
            {
                throw new ArgumentNullException("composite");
            }
            if (composite.BoolValue)
            {
                composite.StringValue += "Suffix";
            }
            return composite;
        }

        public string GetEmployeeName()
        {
            return "ARAVIND KONIKI";
        }

        /// <summary>
        /// multiple two numbers
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <returns></returns>
        public int MultipleTwoNumbers(int a, int b)
        {
            return a * b;
        }

        public int SubtractTwoNumber(int a, int b)
        {
            return a - b;
        }

        public string TestMethod(int a, int b)
        {
            return a.ToString() + b.ToString();
        }
    }
}
